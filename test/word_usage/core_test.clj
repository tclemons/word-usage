(ns word-usage.core-test
  (:require [clojure.test :refer :all]
            [word-usage.core :refer :all]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.pprint :refer [pprint]])
  (:import [java.io StringReader]))

(deftest parse-text-test
  (testing "Parse single line."
    (with-open [rdr (-> (StringReader. "can't less-than foo less-than FOO") (io/reader))]
      (let [usage (parse-text rdr)]
        (is (= (count usage) 3))
        (is (= (get-in usage ["can't" :count]) 1))
        (is (= (get-in usage ["foo" :count]) 2))
        (is (= (get-in usage ["less-than" :count]) 2))))
    )
  (testing "Parse multiple lines."
    (with-open [rdr (-> (StringReader. "foo bar\n FOO  baz baz baz bar\n") (io/reader))]
      (let [usage (parse-text rdr)]
        (is (= (count usage) 3)))))

  (testing "Parse with empty lines."
    (with-open [rdr (-> (StringReader. "\n\nFoo\n\nbar\nfoo\n\nbaz\n\n") io/reader)]
      (let [usage (parse-text rdr)]
        (is (not (contains? usage "")))
        (is (= (count usage) 3))))))

