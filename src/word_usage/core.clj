;; ## Overview
;; This implmentation aims to satisfy the requirements of making efficient use
;; of both memory and runtime.  It composes a data structure representing how
;; each word is used and then from that data structure extracts the most
;; frequently used.
(ns word-usage.core
  (:gen-class)
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.pprint :refer [pprint]]))


;; The delimiter matches against all strings that are not composed of
;; alphabetic characters, hyphens, or apostrophes.
(def text-delimiter #"[^A-Za-z-']+")

(defn parse-text
  "Given a input reader, returns a data structure describing the
   usage statistics of each word.

   The data structure will be of a mapping of the lower-case version of
   the word to its usage statistics, including word count and the various
   forms of the word as the appear in the text.  For example, the text
   \"There there THERE there\" would result in the following:

   `{\"there\", {:count 4 :usages #{\"there\" \"There\" \"THERE\"}}}`
   "
  [rdr]
  ;; `line-seq` processes the input stream lazily on a per-line basis.  This
  ;; allows the file to be processed without reading it in its entirity into
  ;; memory.  Prefiltering is done to remove empty lines and those containing
  ;; only whitespace.
  (loop [lines (->> (line-seq rdr) (map str/trim) (remove empty?))
         usage-stats (transient {})]
    (if (seq lines)
      (let [words (remove empty? (-> lines (first) (str/split text-delimiter)))]
        (recur
          (rest lines)
          (reduce
            (fn [stats word]
              (let [k (-> word str/lower-case)
                    {:keys [count usages]} (get stats k {:count 0 :usages #{}})]
                (assoc! stats k {:usages (conj usages word) :count (inc count)})))
            usage-stats
            words)))
      (persistent! usage-stats))))


(defn most-frequent-words
  "Given a usage data structure produced by `parse-text`, returns a sequence
  of the most-used words.  Each member of the sequence is a set of the various
  capitalizations of the word."
  [usage-stats]
  (let [value-count (comp :count val)]
    (->>
      usage-stats
      (sort-by value-count >)                               ;; Sort the members of the usage map
      (partition-by value-count)                            ;; Partition so that members with identical counts are grouped together
      first                                                 ;; Take the first, which is the sequence of most-used
      (map (comp :usages val)))))


(defn format-usages
  "Given a sequence of usage sets, returns a flattened string that represents a
  comma-delineated list of strings."
  [usages]
  (->>
    usages
    (map #(str/join ", " %))
    (str/join ", ")))

; TODO: print program usage guide if arguments are illegal.
; TODO: read from stdin if no filename is provided.
(defn -main
  "Main function entry."
  [& args]
  (when (pos? (count args))
    (->
      (with-open [rdr (io/reader (first args))]
        (parse-text rdr))
      (most-frequent-words)
      (format-usages)
      (println))))
