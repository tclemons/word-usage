(defproject word-usage "0.1.0-SNAPSHOT"
  :description "Interview problem to determine most common word in a string of text."
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :main ^:skip-aot word-usage.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :plugins [[lein-marginalia "0.9.0"]])
