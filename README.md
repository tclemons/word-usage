# word-usage

This is a coding exercise which derives the most commonly used word(s) from a
given sample text.  The code is written in the Clojure programming language.

## Building

This project requires Java 1.8 and [Leiningen](https://leiningen.org/) to build.
Once Leiningen is installed, run the following to build:

    $ lein test
    $ lein uberjar

## Usage

Run the code by executing the jar as follows:

    $ java -jar target/uberjar/word-usage-0.1.0-standalone.jar <filename>

Where `<filename>` is the text sample to be analyzed.

## Documentation

[Marginalia](https://github.com/gdeer81/marginalia) documentation can be found at
docs/uberdoc.html.


